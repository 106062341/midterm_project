
function Post(root) {
    this.Component = new Component();
    this.on = this.Component.on;
    this.fire = this.Component.fire;

    this.root = root;

    var _t = this;

    //ui
    var total_txt = root.querySelector('#post-product-total-price');
    var price_ele = root.querySelector('#post-product-price');
    var quanty_ele = root.querySelector('#post-product-quantity');
    var image_ele = root.querySelector('#post-product-image');

    price_ele.addEventListener('keyup', function () {
        total_txt.textContent = parseInt(price_ele.value) * parseInt(quanty_ele.value);
    });
    quanty_ele.addEventListener('keyup', function () {
        total_txt.textContent = parseInt(price_ele.value) * parseInt(quanty_ele.value);
    });
    image_ele.addEventListener('change', function () {
        var file = this.files[0];
        if (file) {
            if (file.type.indexOf('image') == -1) {
                window.alert('�ɮ׫��A���~');
                return false;
            }
            if (this.files.length > 1) {
                window.alert('No multiple picture!');
            }
            var _url_ = window.URL || window.webkitURL;
            var img = new Image();
            img.onload = function () {
                console.log(this);
                image_ele.previousElementSibling.textContent = file.name;

                //var imgEle = document.createElement('IMG');
                //imgEle.src = img.src;
                image_ele.nextElementSibling.src = img.src;
            }
            img.src = _url_.createObjectURL(file);
            //this.parentNode.childNodes[0].nodeValue = file.name;
        }
    });

    root.querySelector('#post-confirm').addEventListener('click', function () {
        if (true) { // if is login
            _t.fire('post');
          }
    });

    this.post = function (_user_id) {

        var price = root.querySelector('#post-product-price').value;
        var name = root.querySelector('#post-product-name').value;
        var quantity = root.querySelector('#post-product-quantity').value;
        var unit = root.querySelector('#post-product-unit').value;
        var image_src = root.querySelector('img#product-image').src;
        var classes = root.querySelector('#post-product-label').value;
        var dscp = root.querySelector('#post-product-dscp').value;

        if (parseInt(price) > 0 && parseInt(quantity) > 0 && classes != '') {

            console.log(parseInt(price), parseInt(quantity), classes);

            var newItem = {};
            var user = firebase.database().ref(`host/account/${_user_id}`);

            var newRef = firebase.database().ref('host/goods').child(classes).push();
            var key = newRef.key;

            newItem = {
                id: key,
                owner_id: _user_id,
                name: name,
                price: price,
                image_src: image_src || 'https://images.unsplash.com/photo-1533910534207-90f31029a78e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                quantity: quantity,
                unit: unit,
                classes: classes,
                dscp: dscp,
                rank: 0,
                update: getCurrent()
            }

            newRef.set(newItem);

            newRef = user.child('unsold_product').push();
            newItem.key = newRef.key;
            newRef.set(newItem);

            alert('post success!');

            price = '';
            name = '';
            quantity = '';
            unit = '';
            classes = '';
            dscp = '';

        } else {
            alert('Typerror in post form !');
        }
    }
}