
function Login(formRoot, cartRoot) {
    this.Component = new Component();
    this.on = this.Component.on;
    this.fire = this.Component.fire;

    this.form = formRoot;
    this.cart = cartRoot;
    this.cartList = [];
    this.userAlreadyLogin = {
        login: false,
        id: 0
    };

    var _t = this;
    var _user_id = 0;

    // handle user login or logout's state change
    firebase.auth().onAuthStateChanged(function (userLogin) {
        
        var logoutSpan = document.querySelectorAll('.user-login');
        var loginSpan = document.querySelectorAll('.user-unlogin');

        // if already login
        if (userLogin) {

            firebase.database().ref('host/account').once('value', function (snapshot) {
                var userExisting = false;

                var userList = snapshot.val();
                if (userList) {
                    for (obj in userList) {
                        if (userList[obj]['uid'] && userList[obj]['uid'] === userLogin.uid) {
                            _user_id = userList[obj]['id'];
                            _t.userAlreadyLogin.login = true;
                            _t.userAlreadyLogin.id = userList[obj]['id'];
                            userExisting = true;
                        }
                    }
                }
                if (!userExisting) {

                    var email = formRoot.querySelector('#sign-up-email').value;
                    var phone = formRoot.querySelector('#sign-up-phone').value;
                    var address = formRoot.querySelector('#sign-up-address').value;
                    var psd = formRoot.querySelector('#sign-up-psd').value;
                    var account = '';
                    for (let i = 0; i < email.length; i++) {
                        if (email[i] === '@') break;
                        else account += email[i];
                    }

                    var newRef = firebase.database().ref('host/account').push();
                    var key = newRef.key;
                    newRef.set({
                        account_name: userLogin.displayName || account || 'empty',
                        id: key,
                        uid: userLogin.uid,
                        image_src: userLogin.photoURL || 'https://images.unsplash.com/photo-1533910534207-90f31029a78e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                        email: userLogin.email || email,
                        phone: userLogin.phoneNumber || phone || '0800000000',
                        address: address || 'empty',
                        psd: psd || 'psd',
                        money: 100
                    });
                    _user_id = key;
                    _t.userAlreadyLogin.login = true;
                    _t.userAlreadyLogin.id = key;

                    email = '';
                    phone = '';
                    address = '';
                    psd = '';
                }

                _t.handleUI(true);
            });

            var warned = false;
            for (let i = 0; i < logoutSpan.length; i++) {
                
                if (logoutSpan[i].parentElement.classList.contains('btn-group')) {
                    logoutSpan[i].style.display = 'initial';
                } else {
                    logoutSpan[i].style.display = 'block';
                }

                if (logoutSpan[i].nodeName === 'LABEL') {
                    logoutSpan[i].textContent = userLogin.email;
                } else if (logoutSpan[i].nodeName === 'BUTTON') {
                    logoutSpan[i].addEventListener('click', function (e) {

                        firebase.auth().signOut().then(function () {
                            if (!warned) {
                                alert('Logout success.');
                                window.location.replace('https://mytestfirebase-659ae.firebaseapp.com/');
                                warned = true;
                            }
                        }).catch(function (error) {
                            alert('Logout failed :(');
                        });
                        e.stopPropagation();
                    });
                } else {
                    console.log(logoutSpan[i].nodeName);
                }
                if (loginSpan[i]) loginSpan[i].style.display = 'none';

            }
            // if un login
        } else {

            _t.userAlreadyLogin = false;
            _t.userAlreadyLogin.id = '';
            _user_id = 0;

            for (let i = 0; i < logoutSpan.length; i++) {
                logoutSpan[i].style.display = 'none';
                if (loginSpan[i]) {
                    if (loginSpan[i].parentElement.classList.contains('btn-group')) {
                        loginSpan[i].style.display = 'initial';
                    } else {
                        loginSpan[i].style.display = 'block';
                    }
                }
            }

            _t.handleUI(false);
        }
    });
    // sign with already account
    formRoot.querySelector('#sign-in').addEventListener('click', function () {
       
        var email = formRoot.querySelector('#sign-in-email').value;
        var psd = formRoot.querySelector('#sign-in-psd').value;

        firebase.auth().signInWithEmailAndPassword(email, psd).then(function (result) {
            formRoot.previousElementSibling.click();
            email = '';
            psd = '';
            //window.location.replace("index.html");

        }).catch(function (error) {

            console.error('[AUTH ERROR] sign in failed : ' + error.code + ', ' + error.message);
            alert('Please sign up first!');
        });
    });

    // sign in with google account
    formRoot.querySelector('#sign-in-gg').addEventListener('click', function () {
        
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            //var token = result.credential.accessToken;
            //var user = result.user;
            formRoot.previousElementSibling.click();
            
            //window.location.replace("index.html");
        }).catch(function (error) {
            console.error('[AUTH ERROR] sign in google failed : ' + error.code + ', ' + error.message + ', with error email : ' + error.email + ', ' + error.credential);
            alert('Please sign up first!')
        });
    });
    // register a new account
    formRoot.querySelector('#sign-up').addEventListener('click', function () {
        
        var email = formRoot.querySelector('#sign-up-email').value;
        var psd = formRoot.querySelector('#sign-up-psd').value;

        firebase.auth().createUserWithEmailAndPassword(email, psd).then(function () {
            formRoot.previousElementSibling.click();

        }).catch(function (error) {
            console.error('[AUTH ERROR] sign up failed : ' + error.code + ', ' + error.message);
            alert(error.message);
        });
    });

    //ui
    formRoot.querySelector('.mask-form div.centerlize').addEventListener('click', function (e) {
        var target = e.target;
        if (target.classList.contains('sign-in-identifier')) {
            target.nextElementSibling.classList.remove('active');
            formRoot.querySelector('.sign-in-form').classList.add('active');
            formRoot.querySelector('.sign-up-form').classList.remove('active');
        } else {
            target.previousElementSibling.classList.remove('active');
            formRoot.querySelector('.sign-in-form').classList.remove('active');
            formRoot.querySelector('.sign-up-form').classList.add('active');
        }
        target.classList.add('active');
    });
    
    cartRoot.querySelector('.cartSubmit').addEventListener('click', function () {
        if (_t.userAlreadyLogin.login) {

            var item;

            firebase.database().ref(`host/account/${_t.userAlreadyLogin.id}/cart_list`).once('value', (_snapshot) => {
                var list = _snapshot.val();
                if (!list){
                    alert('No item in cart list.');
                    return;
                }
                for (item in list) {
                    console.log('cart submit - ' + list[item].classes, list[item].id);
                    var target = list[item];
                    var targetRef = firebase.database().ref(`host/goods/${list[item].classes}/${list[item].id}`);
                    if (targetRef) {
                        targetRef.once('value', function (snapshot) {
                            var obj = snapshot.val();
                            console.log(targetRef.key, snapshot.val(), obj)
                            var ownerRef = firebase.database().ref(`host/account/${obj.owner_id}/order_list`);
                            ownerRef.push({
                                buyer_id: _t.userAlreadyLogin.id,
                                goods_id: target.id,
                                name: target.name,
                                price: target.price,
                                image_src: target.image_src,
                                quantity: target.quantity,
                                update: target.update,
                                date: getCurrent()
                            });

                            var unsoldRef = firebase.database().ref(`host/account/${obj.owner_id}/unsold_product`).once('value', function (snapshot) {
                                var obj_ = snapshot.val();
                                for (item_ in obj_) {
                                    if (obj_[item_].id == list[item].id) {
                                        firebase.database().ref(`host/account/${obj.owner_id}/unsold_product/${obj_[item_].key}`).update({
                                            quantity: obj.quantity - target.quantity
                                        });
                                    }
                                }
                            });
                            targetRef.update({
                                quantity: obj.quantity - target.quantity
                            });
                        });
                    }
                }
                var done_list = cartRoot.querySelectorAll('li button.delete');
                var i = 0;
                while (done_list[i]) {
                    done_list[i++].click();
                }
                alert('Success !');
            });
        } else {
            document.querySelector('input#sign-form').checked = true;
        }
    });

    // cart attr
    this.cartAdd = function (cardEle, opt, type=true) {
        var newRef = firebase.database().ref(`host/account/${_t.userAlreadyLogin.id}/cart_list`).push();
        var key = newRef.key;
        var name = cardEle.querySelector('.goods-name').textContent;
        var price = cardEle.querySelector('.goods-buy-number input').value;
        var quantity = cardEle.querySelector('.goods-buy-number input').value;
        var obj = {
            key: key,
            name: name,
            price: price * opt.price,
            quantity: quantity,
            owner_id: opt.owner_id,
            image_src: opt.image_src,
            id: opt.id,
            classes: opt.classes,
            update: opt.update
        };
        console.log('cart - ', obj);
        newRef.set(obj);
    }

    this.getIsLogin = function () {
        console.log('request here', this.userAlreadyLogin, _user_id);
        return this.userAlreadyLogin;
    }

    this.handleUI = function (login) {

        _t.fire('finish_login_process');
        if (login) {
            firebase.database().ref('host/account/' + _user_id).once('value', function (snapshot) {
                var fir = 0, sec = 0;

                var obj = snapshot.val();
                console.log(obj, _user_id);
                _t.userAlreadyLogin.id = _user_id;
                _t.userAlreadyLogin.login = true;
                cartRoot.querySelector('.user-image img').src = obj.image_src;
                cartRoot.querySelector('.user-name span').textContent = obj.account_name;
                cartRoot.querySelector('.user-id span').textContent = obj.id;
                cartRoot.querySelector('#account-money').textContent = obj.money;

                // add item into cart
                var list = cartRoot.querySelector('ul.itemlist');
                var total = cartRoot.querySelector('#total-money');
                if (obj.cart_list) {
                    for (item in obj.cart_list){

                        var newCartItem = new CartItem();
                        var ele = newCartItem.init(list, obj.cart_list[item]);

                        newCartItem.on('removeFromCart', function (child, ele) {

                            firebase.database().ref(`host/account/${_t.userAlreadyLogin.id}/cart_list/${newCartItem.key}`).remove();

                            console.log('lis = ', list, ele);
                            //list.removeChild(ele);
                            console.log('remove new', _t.cartList, this);
                        });

                        fir++;
                        _t.cartList.push(newCartItem);
                        total.textContent = parseInt(total.textContent) + obj.cart_list[item].price;
                    }
                }
                // add item in cart (firebase)
                firebase.database().ref('host/account/' + _user_id + '/cart_list').on('child_added', function (snapshot) {
                    var obj = snapshot.val();
                    console.log('obj = ', obj);
                    sec++;
                    if (sec > fir && obj) {
                        var newCartItem = new CartItem();
                        var ele = newCartItem.init(list, obj);

                        newCartItem.on('removeFromCart', function (child, ele) {

                            firebase.database().ref(`host/account/${_t.userAlreadyLogin.id}/cart_list/${newCartItem.key}`).remove();

                            console.log('lis = ', list, ele);
                            console.log('remove new', _t.cartList, this);
                        });
                        _t.cartList.push(newCartItem);
                        total.textContent = parseInt(total.textContent) + obj.price;
                    } else { console.log(fir, sec);}
                });
                // delete item from cart (firebase)
                firebase.database().ref('host/account/' + _user_id + '/cart_list').on('child_removed', function (snapshot) {
                    var obj = snapshot.val();
                    var i = 0;
                    while (_t.cartList[i]) {
                        if (_t.cartList[i++].key == obj.key) {
                            cartRoot.querySelector('ul.itemlist').removeChild(_t.cartList[i-1].ele);
                            break;
                        }
                    }
                    total.textContent = parseInt(total.textContent)  - obj.price;
                });
            });
        } else {
            cartRoot.querySelector('.user-image img').src = 'https://images.unsplash.com/photo-1533910534207-90f31029a78e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60';
            cartRoot.querySelector('.user-name span').textContent = 'unlogin';
            cartRoot.querySelector('.user-id span').textContent = 'unlogin';
            cartRoot.querySelector('#account-money').textContent = 0;
        }
    }
}
function getCurrent() {

    var d = new Date();
    var m = d.getMonth() + 1;
    var a = d.getDate();
    var h = d.getHours();
    var i = d.getMinutes();
    var s = d.getSeconds();
    d = d.getFullYear().toString() +
        ((m < 10) ? '0' + m : m.toString()) +
        ((a < 10) ? '0' + a : a.toString()) +
        ((h < 10) ? '0' + h : h.toString()) +
        ((i < 10) ? '0' + i : i.toString()) +
        ((s < 10) ? '0' + s : s.toString());
    return d;
}