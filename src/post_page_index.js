
function Index(root) {

    this.root = root;

    this.Component = new Component();
    this.Login = new Login(root.querySelector('.mask > .mask-form'), root.querySelector('#cartInfo'));

    var _t = this;

    this.Post = new Post(root.querySelector('.post-form'));
    this.Post.on('post', function () {
        if (_t.Login.getIsLogin().login) {
            console.log(_t.Login.getIsLogin());
            _t.Post.post(_t.Login.getIsLogin().id);
        } else {
            console.log('unlogin');
        }
    });

    //window.setTimeout(function () {
        /*var login = _t.Login.getIsLogin();
        if (login.login) {//login.login
            _t.Post = new Post(root.querySelector('.post-form'));
            _t.Post.on('post', function (login) {
                _t.Post.post(login.id);
            });*/
    this.Login.on('finish_login_process', function () {
        console.log('fire init');
        if (_t.Login.getIsLogin().id) {
            console.log('fire login init');
            _t.loginID = _t.Login.getIsLogin().id;
            handleUI(_t.Login.getIsLogin().id);
        }
    });
        //}
    //}, 1000);

    this.editListItem = [];
    this.handleUI = handleUI;
    this.loginID = 0;
    this.requestPostProduct = requestPostProduct;
    this.clearDashboard = function (field) {
        console.log(field);
        field.forEach(function (ul) {
            var li = ul.querySelectorAll('li:not(:first-of-type)');
            var i = 0;
            while (li[i]) {
                ul.removeChild(li[i++]);
            }
        });
    }

    function handleUI(id) {
        console.log(id);
        firebase.database().ref('host/account/' + id).once('value', function (snapshot) {
            var obj = snapshot.val();
            console.log(obj);
            root.querySelector('.account-name').textContent = obj.account_name;
            root.querySelector('.account-id').textContent = obj.id;
            root.querySelector('.account-phone').textContent = obj.phone;
            root.querySelector('.account-email').textContent = obj.email;
            root.querySelector('.account-address').textContent = obj.address;
            root.querySelector('.account-image img').src = obj.image_src;
        });

        requestPostProduct(id);

        root.querySelector('#edit-list-submit').onclick = function () {
            
            firebase.database().ref(`host/account/${id}/unsold_product`).once('value', function (snapshot) {
                var obj = snapshot.val();
                var i = 0;

                console.log(obj);
                var update = function (_i) {
                    let j = _i;
                    var vl = _t.editListItem[j].getValue();
                    console.log(j, obj[item].key, vl);

                    firebase.database().ref(`host/account/${id}/unsold_product/${obj[item].key}`).update({
                        price: vl.price,
                        quantity: vl.quantity
                    });
                    console.log(obj[item].classes, obj[item].id);
                    firebase.database().ref(`host/goods/${obj[item].classes}/${obj[item].id}`).update({
                        price: vl.price,
                        quantity: vl.quantity
                    });
                }

                for (item in obj) {
                    console.log(i);
                    update(i);
                    i++;
                }
                window.setTimeout( requestPostProduct(_t.loginID), 1000);
            });
        }
    }

    function requestPostProduct(id) {
        var editBoard = root.querySelector('#editpostlist');
        var unsoldBoard = root.querySelector('#unsoldlist');
        var soldBoard = root.querySelector('#soldlist');

        console.log([editBoard, unsoldBoard, soldBoard]);
        _t.clearDashboard([editBoard, unsoldBoard, soldBoard]);

        firebase.database().ref('host/account/' + id).child('unsold_product').once('value', function (snapshot) {
            var obj = snapshot.val();
            for (item in obj) {
                var newEditList = new EditList(editBoard, obj[item]);
                _t.editListItem.push(newEditList);
                var newUnsoldList = new UnsoldList(unsoldBoard, obj[item]);
            }
        });
        firebase.database().ref('host/account/' + id).child('order_list').once('value', function (snapshot) {
            var obj = snapshot.val();
            for (item in obj) {
                var newSoldList = new SoldList(soldBoard, obj[item]);
            }
        });

    }
}
window.onload = function () {
    const body = document.querySelector('body');

    new Index(body);
};

