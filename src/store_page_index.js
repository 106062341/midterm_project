function Index(root) {

    this.root = root;
    this.detailRoot = root.querySelector('.product');

    this.Component = new Component();
    this.on = this.Component.on;
    this.fire = this.Component.fire;
    this.Login = new Login(root.querySelector('.mask > .mask-form'), root.querySelector('#cartInfo'));
    this.Login.on('finish_login_process', function () {
        console.log('fire init');
    });

    var _t = this;
    this.Goodsgrid = new Goodsgrid(root.querySelector('section.goods-table'));
    this.Goodsgrid.on('addToCart', function (child, cardEle, opt, type) {
        console.log(arguments, cardEle, opt);
        _t.Login.cartAdd(cardEle, opt, type);
    });
    this.Goodsgrid.on('show_detail', function (child, goods_id, goods_class) {

        root.querySelector('input#product-detail').checked = true;
        firebase.database().ref('host/goods/' + goods_class + '/' + goods_id).once('value', function (snapshot) {

            var obj = snapshot.val();
            console.log(goods_id, goods_class, obj);
            root.querySelector('.product-name').textContent = obj.name;
            root.querySelector('.product-price span').textContent = obj.price;
            root.querySelector('.product-sold span').textContent = obj.quantity;
            root.querySelector('.product-dscp p').textContent = obj.dscp;
            root.querySelector('.product-image img').src = obj.image_src;
        });
    });


    this.Login.on('finish_login_process', function () {
        _t.Goodsgrid.getGoods(root.querySelector('.goods-grid-container'), 'total', Infinity);
    });

    /*window.setTimeout(function () {
        _t.Goodsgrid.getGoods(root.querySelector('.goods-grid-container'), 'total', Infinity);
    }, 1000);*/

    root.querySelector('.search-bar button').addEventListener('click', function () {
        var keyword = root.querySelector('.search-bar input').value.replace(/ /g, '');
        if (keyword == '') keyword = 'total';

        _t.Goodsgrid.getGoods(root.querySelector('.goods-grid-container'), keyword, Infinity);
    });
}

window.onload = function () {
    const body = document.querySelector('body');

    new Index(body);
};