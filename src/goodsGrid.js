
function Goodsgrid(root) {

    this.Component = new Component();
    this.on = this.Component.on;
    this.fire = this.Component.fire;
    this.GridItem = [];

    this.root = root;
    this.rootProduct = document.querySelector('.product');

    this.goodsCandyRef = firebase.database().ref('/host/goods/candy');
    this.goodsCookieRef = firebase.database().ref('/host/goods/cookie');
    this.class_goods = {
        candy: [],
        cookie: [],
        total: []
    }

    var _t = this;
    // keyword detection
    this.keywordDetection = function(key){
        return this.class_goods[key] || false;
    }
    //getGoods(root.querySelector('goods-grid-container', 'total', 8));
    function getCandyGoods() {
        // candy part
        var _can_ref = firebase.database().ref('host/goods/candy');
        _can_ref.once('value')
            .then(function (snapshot) {
                var obj = snapshot.val();
                var fir = 0, sec = 0;
                for (item in obj) {
                    _t.class_goods.candy.push(obj[item]);
                    _t.class_goods.total.push(obj[item]);
                    console.log(_t.class_goods.candy);
                    fir++;
                }

                _can_ref.on('child_added', function (snapshot) {
                    var obj = snapshot.val();
                    sec++;
                    if (sec > fir) {
                        _t.class_goods.candy.push(obj);
                        _t.class_goods.total.push(obj);
                        console.log('new candy', _t.class_goods.candy);
                    }
                });
            })
            .catch(e => console.error(e.message));
    }
    function getCookieGoods() {
        // cookie part
        var _cok_ref = firebase.database().ref('host/goods/cookie');
        _cok_ref.once('value')
            .then(function (snapshot) {
                var obj = snapshot.val();
                var fir = 0, sec = 0;
                for (item in obj) {
                    _t.class_goods.cookie.push(obj[item]);
                    _t.class_goods.total.push(obj[item]);
                    console.log(_t.class_goods.cookie);
                    fir++;
                }

                _cok_ref.on('child_added', function (snapshot) {
                    var obj = snapshot.val();
                    sec++;
                    if (sec > fir) {
                        _t.class_goods.cookie.push(obj);
                        _t.class_goods.total.push(obj);
                        console.log('new cookie', _t.class_goods.cookie);
                    }
                });
            })
            .catch(e => console.error(e.message));
    }
    getCandyGoods();
    getCookieGoods()
    // sort arrays, sort by date
    this._sort = function (arr) {
        if (arr) {
            arr.sort(function (a, b) {
                return parseInt(b.update) -  parseInt(a.update);
            });
        } else {
            console.error('[GOODS TABLE] wrong arr : ', arr);
        }
    }
    this.getGoods = getGoods;
    this.clearTable = function () {
        console.log('clear goes here');
        while (this.GridItem[0]) {
            console.log(this.GridItem[0]);
            this.GridItem[0].remove();
            this.GridItem.shift();
            console.log(this.GridItem);
        }
        
    }
    function getGoods(root, _classes, _number) {
        console.log(_t.class_goods, _classes, _t.class_goods[_classes]);

        if (!_t.class_goods[_classes]) {
            alert('No request products. Try another keyword.');
            _t.root.querySelector('.search-bar input').focus();
            return;
        }

        var classes = (this.keywordDetection(_classes)) ? _classes : 'total';

        this.clearTable();
        this._sort(_t.class_goods[classes]);

        var number = Math.min(_number, _t.class_goods[classes].length);
        
        for (let i = 0; i < number; i++) {
            var newCard = new GoodsCard();
            var ele = newCard.init(root, {
                name: _t.class_goods[classes][i].name,
                price: this.class_goods[classes][i].price,
                image_src: this.class_goods[classes][i].image_src,
                quantity: this.class_goods[classes][i].quantity,
                classes: _t.class_goods[classes][i].classes,
                id: this.class_goods[classes][i].id,
                index: i
            });
            newCard.on('addToCart', function (child, cardEle, index) {
                var opt = {
                    owner_id: _t.class_goods[classes][index].owner_id,
                    id: _t.class_goods[classes][index].id,
                    image_src: _t.class_goods[classes][index].image_src,
                    price: _t.class_goods[classes][index].price,
                    update: _t.class_goods[classes][index].update,
                    classes: _t.class_goods[classes][index].classes
                }
                _t.fire('addToCart', cardEle, opt, true);
            });
            newCard.on('show_detail', function (child, goods_id, goods_classes) {
                _t.fire('show_detail', goods_id, goods_classes);
            });
            this.GridItem.push(ele);
        }
    }
    
}