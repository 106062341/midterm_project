
function EditList(root, obj) {

    this.Component = new Component();
    this.on = this.Component.on;
    this.fire = this.Component.fire;

    this.root = root;

    this.ele = document.createElement('li');
    this.ele.innerHTML = '<span class="p-2-2">' + obj.name + ' </span><input type="text" value=' + obj.price + ' class="c-primary p-2-2 fs-sm"><input type="text" value=' + obj.quantity + '  class="c-primary p-2-2 fs-sm">';
    this.key = obj.key;

    root.appendChild(this.ele);

    var _t = this;
    this.getValue = function () {
        var vl = this.ele.querySelectorAll('input');
        return {
            price: vl[0].value,
            quantity: vl[1].value
        }
    }
}

function SoldList(root, obj) {

    this.Component = new Component();
    this.on = this.Component.on;
    this.fire = this.Component.fire;

    this.root = root;

    this.ele = document.createElement('li');
    this.ele.innerHTML = '\
                    <span>' + obj.name + ' </span>\
                    <span class="d-md-blk">' + parseInt(obj.price) / parseInt(obj.quantity) + '</span>\
                    <span class="d-md-blk">' + obj.quantity + ' </span>\
                    <span>' + obj.price + ' </span>\
                    <span class="d-md-blk">' + obj.update + '</span>\
                    <span>' + obj.date + '</span>\
                    <span>' + obj.buyer_id + '</span>';

    root.appendChild(this.ele);
}

function UnsoldList(root, obj) {

    this.Component = new Component();
    this.on = this.Component.on;
    this.fire = this.Component.fire;

    this.root = root;

    this.ele = document.createElement('li');
    this.ele.innerHTML = '\
                    <span>' + obj.name + ' </span>\
                    <span class="d-md-blk">' + obj.price  + '</span>\
                    <span class="d-md-blk">' + obj.quantity + ' </span>\
                    <span>' + parseInt(obj.price) * parseInt(obj.quantity) + ' </span>\
                    <span class="d-md-blk">' + obj.update + '</span>\
                    <span></span>\
                    <span></span>';

    root.appendChild(this.ele);
}