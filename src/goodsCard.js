﻿
function GoodsCard() {

    this.Component = new Component();
    this.on = this.Component.on;
    this.fire = this.Component.fire;

    this.root = 0;
    this.ele =  document.createElement('div');
    this.ele.classList.add('card', 'mb-2', 'p-2-2', 'bgc-mix');
    this.ele.innerHTML = '<div class="goods-image mb-2"></div>\
            <h3 class="goods-name fs-md">商品名稱</h3>\
            <small class="goods-id">id</small>\
            <div class="goods-info mb-2"><strong class="goods-price fs-md c-primary">3000</strong><span>剩下&nbsp;<span class="goods-sold">200</span>&nbsp;件</span></div>\
            <div class="centerlize">\
                <div class="goods-buy-number">\
                    <input type="text" class="p-1-1 fs-md" value=0 min="0" step="1">\
                    <button class="btn other-btn p-1-1 add"><i class="fas fa-plus"></i></button>\
                    <button class="btn other-btn p-1-1 sub"><i class="fas fa-minus"></i></button>\
                </div>\
                <button class="btn emp p-2-2 addToCartBtn">加入購物車</button>\
            </div>';
    this.index = 0;
    this.goods_id = '';
    this.goods_class = '';

    this.init = function (root, obj) {

        this.root = root;
        root.appendChild(this.ele);

        this.ele.querySelector('.goods-name').textContent = obj.name || 'EMPTY';
        this.ele.querySelector('.goods-image').style.backgroundImage = 'url(' + obj.image_src + ')' || 'url("https://images.unsplash.com/photo-1533910534207-90f31029a78e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60")';
        this.ele.querySelector('.goods-price').textContent = '$ ' + (obj.price || 'UNKNOWNPRICE');
        this.ele.querySelector('.goods-id').textContent = obj.id || 'UNKNOWNID';
        this.ele.querySelector('.goods-sold').textContent = obj.quantity;
        this.index = obj.index;
        this.goods_id = obj.id;
        this.goods_class = obj.classes;

        var _t = this;
        var _ele = this.ele;
        // add & sub
        this.ele.querySelector('.goods-buy-number i.fa-plus').onclick = function () {
                _ele.querySelector('.goods-buy-number input').value = parseInt(_ele.querySelector('.goods-buy-number input').value) + 1;
        }
        this.ele.querySelector('.goods-buy-number i.fa-minus').onclick = function () {
            if (_ele.querySelector('.goods-buy-number input').value > 0) {
                _ele.querySelector('.goods-buy-number input').value = parseInt(_ele.querySelector('.goods-buy-number input').value) - 1;
            }
        }

        this.ele.querySelector('.goods-image').onclick = function () {
            _t.fire('show_detail', _t.goods_id,  _t.goods_class);
        }

        this.ele.querySelector('.addToCartBtn').onclick = function (e) {
            if (_ele.querySelector('.goods-buy-number input').value > 0) {
                _t.fire('addToCart', _t.ele, _t.index);
            }
        }
        return this.ele;
    }
    this.set = function(key, value){
        var key = key.toLittleCase();
        this.ele.querySelector('.goods-'+key).textContent = value;
    }
    this.remove = function () {
        this.root.removeChild(this.ele);
    }
}