
function Index(root)  {

    this.root = root;
    this.detailRoot = root.querySelector('.product');

    this.Component = new Component();
    this.Login = new Login(root.querySelector('.mask > .mask-form'), root.querySelector('#cartInfo'));
    this.Login.on('finish_login_process', function () {
        console.log('fire init');
    });

    var _t = this;
    this.Goodsgrid = new Goodsgrid(root.querySelector('section.goods-table'));
    this.Goodsgrid.on('addToCart', function (child, cardEle, opt, type) {
        console.log(arguments, cardEle, opt, type);
        _t.Login.cartAdd(cardEle, opt, type);
    });
    this.Goodsgrid.on('show_detail', function (child, goods_id, goods_class) {

        root.querySelector('input#product-detail').checked = true;
        firebase.database().ref('host/goods/' + goods_class + '/' + goods_id).once('value', function (snapshot) {

            var obj = snapshot.val();
            console.log(goods_id, goods_class, obj);
            root.querySelector('.product-name').textContent = obj.name;
            root.querySelector('.product-price span').textContent = obj.price;
            root.querySelector('.product-sold span').textContent = obj.quantity;
            root.querySelector('.product-dscp p').textContent = obj.dscp;
            root.querySelector('.product-image img').src = obj.image_src;
        });
    });

    window.setTimeout(function () {
        _t.Goodsgrid.getGoods(root.querySelector('.goods-grid-container'), 'total', 8);
    }, 1000);
    //ui
    /*var featureEle = root.querySelector('section.feature');
    featureEle.addEventListener('click', function (e) {
        if (e.target.classList.contains('angle')) {
            var slide = featureEle.querySelector('.rotate-window');
            if (e.target.classList.contains('right')) {
                slide.style.marginLeft = 
            } else {

            }
        }
    });*/
}
window.onload = function () {
    const body = document.querySelector('body');

    new Index(body);
};

