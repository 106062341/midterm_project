
function Component() {
    
    var eventPair = {};

    this.on = function (emitter, callback) {
        if (typeof emitter === 'string' && typeof callback === 'function') {
            eventPair[emitter] = callback;
        }else {
            console.error('[COMPONENT ERROR] wrong type : ' + emitter + ', ' + callback);
        }
    }
    this.fire = function(event, ...args) {
        if (eventPair[event]) {
            eventPair[event](this, ...args);
        } else {
            console.error('_COMPONENT ERROR_ wrong type : ' + event );
        }
    }
}