
function CartItem() {

    this.Component = new Component();
    this.on = this.Component.on;
    this.fire = this.Component.fire;

    this.ele = document.createElement('li');
    this.ele.classList.add('item', 'p-2-3', 'fs-sm');
    this.ele.innerHTML = '<img alt= "item image" width= 60 height= 60 >\
            <h5 class="p-2-2">item name</h5>\
            <span class="p-2-2">item price</span>\
            <button class="btn other-btn inf"><a><i class="fas fa-info-circle"></i></a></button>\
            <button class="btn other-btn delete"><i class="fas fa-times"></i></button>';
    this.key = 0;

    var _t = this;
    this.init = function (root, obj) {

        root.appendChild(this.ele);

        this.ele.querySelector('img').src = obj.image_src || 'https://images.unsplash.com/photo-1531361171768-37170e369163?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=675&q=80';
        this.ele.querySelector('h5').textContent = obj.name || 'EMPTY';
        this.ele.querySelector('span').textContent = obj.price || 'UNKNOWN';
        this.key = obj.key;

        this.ele.querySelector('button.delete').onclick = function (e) {
            console.log(_t.ele);
                _t.fire('removeFromCart', _t.ele);
        }
        this.ele.querySelector('button.inf').onclick = function (e) {
                alert('direct to product page');
        }
        
        return this.ele;
    }
}