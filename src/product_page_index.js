
function Index(root) {

    this.root = root;

    this.Component = new Component();
    this.on = this.Component.on;
    this.fire = this.Component.fire;

    this.Login = new Login(root.querySelector('.mask > .mask-form'), root.querySelector('#cartInfo'));

    var _t = this;
    
    this.Login.on('finish_login_process', function () {
        console.log('fire init');
        _t.handleUI();
    });

    this.handleUI = function() {
        var href = window.location.href, id ='', classes ='';
        var trim = window.location.hostname + window.location.pathname;
        href = href.replace(trim, '');
        var i = 0, isID =true;
        while (href[i]) {
            if (href[i] == '&') {
                isID = false;
                continue;
            }
            if (isID) {
                id += href[i];
            } else {
                classes += href[i];
            }
        }
        alert(href, id, classes);

        firebase.database().ref('host/goods'+classes+'/'+id ).once('value', function (snapshot) {
            var obj = snapshot.val();
            console.log(obj);
            root.querySelector('.product-name').textContent = obj.name;
            root.querySelector('.product-price span').textContent = obj.price;
            root.querySelector('.product-sold span').textContent = obj.quantity;
            root.querySelector('.product-dscp p').textContent = obj.dscp;
            root.querySelector('.product-image img').src = obj.image_src;
        });
    }
}
window.onload = function () {
    const body = document.querySelector('body');

    new Index(body);
};

