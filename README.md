# Software Studio 2019 Spring Midterm Project

## Topic
* Project Name : shopping store
* Key functions (add/delete)
    1. shopping website
    
* Other functions (add/delete)

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|Firebase Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://mytestfirebase-659ae.firebaseapp.com/

# Components Description : 
1. 前往 user dashboard : 登入後左方拉頁打開 -> '前往個人首頁'
2. 前往 product page : 登入後 -> 底下 或 '更多商品'
3. 執行 shopping pipeline : 登入後 -> 圖片卡中先決定好購買數量 -> 按'加入購物車 -> 左方拉頁打開，確認清單 -> 按'確認結帳'完成購買
4. 點擊上方navbar的sign in或sign up可登入；可用google登入或註冊新帳號或使用下列的測試帳號:帳號: a@g.com, 密碼: asdasd

# Other Functions Description(1~10%) : 

## Security Report (Optional)
